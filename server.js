const dotenv = require("dotenv");
dotenv.config()
const express = require("express");
const fileUpload = require('express-fileupload');
const cors = require("cors");

const app = express();

global.__basedir = __dirname;

var corsOptions = {
  origin: "http://localhost:8080"
  //origin: "https://beta.cryptomonkeyproduction.com"
};
app.use(cors(corsOptions));
// parse requests of content-type - application/json
app.use(express.json());
const path = require('path')
app.use(express.static(path.join(__dirname, 'public'))); //to access the files in public folder
console.log(__dirname);
app.use(fileUpload());
// parse requests of content-type - application/x-www-form-urlencoded
app.use(express.urlencoded({ extended: true }));


const db = require("./models");
const Role = db.role;

/*
function initial() {
  Role.create({
    id: 1,
    name: "user"
  });
  Role.create({
    id: 2,
    name: "admin"
  });
  Role.create({
    id: 3,
    name: "daoOwner"
  })
}
db.sequelize.sync({ force: true })
  .then(() => {
    console.log("Drop and re-synced db.");
    initial();
  })
  .catch((err) => {
    console.log("Failed to sync db: " + err.message);
  });*/
  
const PORT = process.env.PORT || 3000;

// Import Routes
require("./routes/user.routes.js")(app);
require("./routes/auth.routes.js")(app);
require("./routes/dao.routes.js")(app);
require("./routes/categorie.routes.js")(app);
require("./routes/payment.routes.js")(app);
require("./routes/transactions.routes.js")(app);
require("./routes/collection.routes.js")(app);
require("./routes/wallet.routes.js")(app);
require("./routes/image.route.js")(app);
require("./routes/pdf.routes.js")(app);

app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}.`);
});
