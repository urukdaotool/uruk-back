const { authJwt, dao } = require("../middleware");
const controller = require("../controllers/pdf.controller");


module.exports = function (app) {
  app.use(function (req, res, next) {
    res.header(
      "Access-Control-Allow-Headers",
      "x-access-token, Origin, Content-Type, Accept"
    );
    next();
  });
  app.post(
    "/api/pdf/create",
    [
      authJwt.verifyToken,
      dao.hasDaoOwnerRole
    ],
    controller.create
  );

  app.post(
    "/api/delete/pdf",
    [
      authJwt.verifyToken,
      dao.hasDaoOwnerRole
    ],
    controller.delete
  )

 
};