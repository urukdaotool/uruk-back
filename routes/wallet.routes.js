const { authJwt, dao } = require("../middleware");
const controller = require("../controllers/wallet.controller.js");
const service = require('../services/token.service');

module.exports = function (app) {
  app.use(function (req, res, next) {
    res.header(
      "Access-Control-Allow-Headers",
      "x-access-token, Origin, Content-Type, Accept"
    );
    next();
  });
  app.post(
    "/api/wallet/add",
    [
      authJwt.verifyToken,
      dao.hasDaoOwnerRole
    ],
    controller.add
  );

  app.get(
    "/api/wallets",
    [
      authJwt.verifyToken,
      dao.hasDaoOwnerRole
    ],
    controller.findAllWallets
  )

  app.get(
    "/api/wallet/:id",
    [
      authJwt.verifyToken,
      dao.isDaoOwner
    ],
    controller.findOne
  )

  app.post(
    "/api/getwallettoken",
    [
      authJwt.verifyToken,
      dao.hasDaoOwnerRole 
    ],
    controller.walletToken
  )

  app.post(
    "/api/nft",
    [
      //authJwt.verifyToken,
      //dao.hasDaoOwnerRole
    ],
    controller.nftWallet
  )

  app.post(
    "/api/wallets/delete",
    [
      authJwt.verifyToken,
      dao.isDaoOwner
    ],
    controller.delete
  )
};