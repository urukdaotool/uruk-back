const controller = require("../controllers/transaction.controller.js");
const { authJwt, tx, dao } = require("../middleware");

module.exports = (app) => {
  app.use(function (req, res, next) {
    res.header(
      "Access-Control-Allow-Headers",
      "x-access-token, Origin, Content-Type, Accept"
    );
    next();
  });

  app.post(
    "/api/transactions",
    [
      //authJwt.verifyToken,
      //tx.isUser
    ],
    controller.getApiTx
  ),

  app.post(
    "/api/create/transactions",
    [
      authJwt.verifyToken,
      dao.hasDaoOwnerRole
    ],
    controller.create
  ),
  app.post(
    "/api/delete/transactions",
    [
      authJwt.verifyToken,
      dao.hasDaoOwnerRole
    ],
    controller.delete
  )
  app.get(
    "/api/transactions",
    [
      authJwt.verifyToken,
      dao.hasDaoOwnerRole
    ],
    controller.getTx
  )
}