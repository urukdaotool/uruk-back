const { Sequelize } = require('sequelize');
const dbConfig = require("../config/db.config.js");
// Squelize 
const sequelize = new Sequelize(dbConfig.DB, dbConfig.USER, dbConfig.PASSWORD, { 
  dialect: dbConfig.dialect, 
  host : dbConfig.HOST,
  operatorAliases: false,
  pool: {
    max: dbConfig.pool.max,
    min: dbConfig.pool.min,
    acquire: dbConfig.pool.acquire,
    idle: dbConfig.pool.idle
  }
})

const db = {};
db.Sequelize = Sequelize;
db.sequelize = sequelize;
db.user = require("./user.model.js")(sequelize, Sequelize);
db.role = require("./role.model.js")(sequelize, Sequelize);
db.dao = require("./dao.model.js")(sequelize, Sequelize);
db.wallet = require("./wallet.model.js")(sequelize, Sequelize); 
db.categories = require("./categorie.model.js")(sequelize, Sequelize);
db.transaction = require("./transaction.model")(sequelize, Sequelize);
db.payments = require("./payment.model.js")(sequelize, Sequelize);
db.images = require("./image.model.js")(sequelize, Sequelize);
db.pdfs = require("./pdf.model.js")(sequelize, Sequelize);
db.collection = require("./collection.model.js")(sequelize, Sequelize);

// Relation
// ROLE - USER
db.role.belongsToMany(db.user, {
  through: "user_roles",
  foreignKey: "roleId",
  otherKey: "userId"
});
db.user.belongsToMany(db.role, {
  through: "user_roles",
  foreignKey: "userId",
  otherKey: "roleId"
});

// IMAGE - USER
db.user.hasMany(db.images);
db.images.belongsTo(db.user);

// IMAGE - DAO
db.dao.hasMany(db.images);
db.images.belongsTo(db.dao);

// WALLET - DAO
db.dao.hasMany(db.wallet);
db.wallet.belongsTo(db.dao);

// DAO - USER
db.dao.belongsToMany(db.user, {
  through: "user_daos",
  foreignKey: "daoId",
  otherKey: "userId"
});
db.user.belongsToMany(db.dao, {
  through: "user_daos",
  foreignKey: "userId",
  otherKey: "daoId"
});

// DAO - CATEGORIES
db.dao.hasMany(db.categories, { as: "categories"});

db.categories.belongsTo(db.dao, {
  foreignKey: "daoId",
  as: "dao"
});

//DAO - COLLECTION
db.dao.hasOne(db.collection);
db.collection.belongsTo(db.dao)

// DAO - PDF
db.dao.hasMany(db.pdfs);
db.pdfs.belongsTo(db.dao);

// CATEGORIES - TRANSACTIONS
db.categories.hasMany(db.transaction)
db.transaction.belongsTo(db.categories)
/*
// PAYMENT - CATEGORIE
db.categories.hasMany(db.payments, { as: "payments"});

db.payments.belongsTo(db.categories, {
  foreignKey: "categorieId",
  as: "categorie"
})*/

// Roles
db.ROLES = ['user', 'admin', 'daoOwner'];

module.exports = db;
