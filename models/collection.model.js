module.exports = (sequelize, Sequelize) => {
    const Collection = sequelize.define("collections", {
      id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
      },
      helloMoonCollectionId: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      name: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      floorPrice: {
        type: Sequelize.STRING,
        allowNull: true
      },
      totalSupply: {
        type: Sequelize.INTEGER,
        allowNull: false
      },
      market_cap_sol: {
        type: Sequelize.FLOAT,
        allowNull: true
      },
      market_cap_usd: {
        type: Sequelize.FLOAT,
        allowNull: true
      },
      ath: {
        type: Sequelize.STRING,
        allowNull: true
      },
      atl: {
        type: Sequelize.STRING,
        allowNull: true
      },
      total_volume: {
        type: Sequelize.STRING,
        allowNull: true
      },
      isDeleted: {
        type: Sequelize.BOOLEAN,
        defaultValue: false
      }
    });
    return Collection;
  };