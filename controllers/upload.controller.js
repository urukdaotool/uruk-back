const { user } = require("../models");
const db = require("../models");
const Image = db.images;
const Dao = db.dao;
const User = db.user;

exports.uploadFiles = async (req, res) => {
  try {
    if (!req.files) {
      return res.status(500).send({ message: `You must select a file.` });
    }

    const { fileUpload } = req.files;

    const userId = req.body.userId;
    const daoId = req.body.daoId;
    const dao = await Dao.findByPk(daoId);
    const user = await User.findByPk(userId);
    const now = new Date();
    const currentDate = now.getFullYear()+"-"+(now.getMonth()+1)+"-"+ now.getDate();

    if(daoId) {
      fileUpload.mv(`${__basedir}/public/images/dao/${daoId}_${currentDate}_${fileUpload.name}`, function (err) {
        if (err) {
          return res.status(500).send({ msg: err.message });
        }
        // If does not have image mime type prevent from uploading
        if (/^fileUpload/.test(fileUpload.mimetype)) return res.sendStatus(400);
      });

      // creating image
      const image = await Image.create({
        url : `${daoId}_${currentDate}_${fileUpload.name}`
      })
      // add image to DAO
      dao.setImages(image);

      // returing the response with file path and name
      return res.send({ name: fileUpload.name, path: `${daoId}_${currentDate}_${fileUpload.name}` });
    }

    if(userId) {
      fileUpload.mv(`${__basedir}/public/images/user/${fileUpload.name}_${userId}_${now}`, function (err) {
        if (err) {
          return res.status(500).send({ msg: err.message });
        }
        // If does not have image mime type prevent from uploading
        if (/^fileUpload/.test(fileUpload.mimetype)) return res.sendStatus(400);
      });
      const image = await Image.create({
        url : `${userId}_${currentDate}_${fileUpload.name}`
      })

      user.setImages(image);

      // returing the response with file path and name
      return res.send({ name: fileUpload.name, path: `${userId}_${currentDate}_${fileUpload.name}` });
    }
  } catch (err) {
    console.log(err);
  }
}
