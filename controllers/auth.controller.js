const db = require("../models");
const config = require("../config/auth.config");
const axios = require('axios');
const User = db.user;
const Wallet = db.wallet;
const Role = db.role;
const Dao = db.dao;
const Pdf = db.pdfs;
const Op = db.Sequelize.Op;
const collectionService = require('../services/collection.service');
const TokenService = require('../services/token.service.js');
var jwt = require("jsonwebtoken");
const LAMPORT = 1000000000;

exports.signup = async (req, res) => {
  try {
    // create user
    const user = await User.create({
      name: req.body.name || null,
      discord_name: req.body.discord_name || null,
      address: req.body.address,
      email: req.body.email || null
    })
    user.update({ isActive: true});
    // Create Dao
    if (req.body.roles[0] === "daoOwner") {
      const dao = await Dao.create({
        name: req.body.daoName
      })
      user.setDaos(dao);

      // assign roles
      const roles = await Role.findAll({
        where: {
          name: {
            [Op.or]: req.body.roles
          }
        }
      })
      user.setRoles(roles);

      //create main wallet
      const wallet = await Wallet.create({
        name: "Main Wallet",
        address: req.body.address,
        nativeBalance: 0
      })

      // Get native balance for the wallet
      const url = `https://api.helius.xyz/v0/addresses/${req.body.address}/balances?api-key=${process.env.HELIUS_API_KEY}`;

      const { data } = await axios.get(url);

      let newWallet = await wallet.update({ nativeBalance: data.nativeBalance / LAMPORT }, {
        where: {
          daoId: dao.id
        }
      })

      dao.addWallets(wallet);

      // create collection
      const collection = await collectionService.getCollectionAndCreateInDb(req.body.helloMoonCollectionId);

      //console.log(collection);

      dao.setCollection(collection);

      var token = jwt.sign({ id: user.id }, config.secret, {
        expiresIn: 86400 // 24 hours
      });

      var authorities = [];
      user.getRoles().then(roles => {
        for (let i = 0; i < roles.length; i++) {
          authorities.push("ROLE_" + roles[i].name.toUpperCase());
        }
        user.getDaos().then(daos => {
          res.status(200).send({
            id: user.id,
            name: user.name,
            email: user.email,
            roles: authorities,
            accessToken: token,
            daos: daos,
            collection: collection
          });
        })
      });
    }
  } catch (err) {
    console.log(err.message);
    res.status(401).send("message " + err.message);
  }
};

exports.signin = async (req, res) => {
  console.log(req.body.address);
  try {
    const user = await User.findOne({
      where: {
        address: req.body.address
      }
    })

    user.update({ isConnected: 1});
    // check if user is deleted
    if(!user.isActive) return res.status(404).send({ message: "User Not found." });

    var token = jwt.sign({ id: user.id }, config.secret, {
      expiresIn: 86400 // 24 hours
    });

    var authorities = [];

    const roles = await user.getRoles();

    for (let i = 0; i < roles.length; i++) {
      authorities.push("ROLE_" + roles[i].name.toUpperCase());
    }

    const daos = await user.getDaos();

    const collection = await daos[0].getCollection({
      where: {
        daoId : daos[0].id
      }
    });

    const image = await daos[0].getImages();

    const pdfs = await daos[0].getPdfs({
      where: {
        daoId : daos[0].id
      }
    })

    console.log(image);

    res.status(200).send({
      id: user.id,
      name: user.name,
      email: user.email,
      roles: authorities,
      accessToken: token,
      daos: daos,
      collection: collection,
      image: image,
      pdfs: pdfs
    });
  } catch (err) {
    console.log(err.message)
    return res.status(404).send({ message: "User Not found." });
  }
};

exports.logout = async (req, res) => {
  const userId = req.body.userId

  try {
    const user = await User.findByOk(userId);

    user.update({ isConnected: 0});

  } catch(err) {

  }

}
