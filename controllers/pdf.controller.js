const db = require("../models");
const Dao = db.dao;
const Image = db.images
const Pdf = db.pdfs;
const Categorie = db.categories;
const Transactions = db.transaction;
const Op = db.Sequelize.Op;
const fs = require('fs');
const PdfPrinter = require('pdfmake');
const moment = require("moment");


exports.create = async (req, res) => {

    const daoId = req.body.daoId;
    const pdfName = req.body.pdfName;

    const dao = await Dao.findByPk(daoId);

    const image = await Image.findOne({
        where: {
            daoId: daoId
        }
    });

    const now = new Date();
    const currentDate = now.getFullYear() + "-" + (now.getMonth() + 1) + "-" + now.getDate();

    var fonts = {
        Roboto: {
            normal: `${__basedir}/fonts/Roboto-Regular.ttf`,
            bold: `${__basedir}/fonts/Roboto-Medium.ttf`,
            italics: `${__basedir}/fonts/Roboto-Italic.ttf`,
            bolditalics: `${__basedir}/fonts/Roboto-LightItalic.ttf`
        }
    };

    const printer = new PdfPrinter(fonts);

    var docDefinition = {
        // a string or { width: number, height: number }
        pageSize: 'A4',
        // by default we use portrait, you can change it to landscape if you wish
        pageOrientation: 'landscape',
        // [left, top, right, bottom] or [horizontal, vertical] or just a number for equal margins
        pageMargins: [40, 60, 40, 60],
        content: [
            {
                columns: [
                    {
                        image: `${__basedir}/public/images/dao/${image.url}`,
                        width: 120,
                        height: 120
                    },
                    {
                        text: dao.name,
                        style: "header",
                        fontSize: 22
                    }
                ]
            },
            "\n\n"
        ]
    };

    try {
        const categories = await Categorie.findAll({
            where: {
                daoId: daoId
            }
        });

        categories.forEach(async (categorie) => {
            const txByCategories = await Transactions.findAll({ where: { categoryId: categorie.id } });
            docDefinition.content.push({ text: categorie.name, style: "header", bold: true });
            txByCategories.forEach(tx => {
                let dateTx = moment.unix(tx.blockTime);
                dateTx = moment(dateTx).format("DD-MM-YYYY");
                let table = {
                    style: {fontSize: 10},
                    table: {
                        body: [
                            ["Type", "Description", "Source", "wallet", "Date"],
                            [tx.type, tx.description, tx.source, tx.wallet, dateTx]
                        ]
                    }
                }
                docDefinition.content.push(table);
            })
            docDefinition.content.push("\n\n");
        })
        console.log(docDefinition);

        const pdfCreated = await Pdf.create({
            name: pdfName,
            url: `${daoId}_${currentDate}_${pdfName}.pdf`
        })

        dao.addPdfs(pdfCreated);

        var pdfDoc = printer.createPdfKitDocument(docDefinition);

        pdfDoc.pipe(fs.createWriteStream(`${__basedir}/public/pdf/${daoId}_${currentDate}_${pdfName}.pdf`));
        pdfDoc.end();

        res.status(200).send({
            pdf: pdfCreated,
            pdfName: `${daoId}_${currentDate}_${pdfName}.pdf`,
            message: "pdf created"
        })
    } catch (err) {
        console.log(err);
    }

}

exports.delete = async (req, res) => {
    const daoId = req.body.daoId;
    const pdfUrl = req.body.pdfUrl;

    try {
        const pdf = await Pdf.findOne({
            where: {
                url: pdfUrl
            }
        });
        await pdf.update({ daoId: null }, {
            where: {
                daoId: daoId
            }
        });
        res.status(200).send({
            message: "Pdf deleted !"
        })

    } catch (err) {
        console.log(err.message);
        res.status(500).send({
            err: err.message,
            message: "An error occur for deleting the pdf."
        })
    }
}