const db = require("../models");
const { compareAndUpdateCollection } = require("../services/collection.service");
const Collection = db.collection;
const Dao = db.dao;
const Op = db.Sequelize.Op;

exports.findOne = async (req, res) => {
    const daoId = req.query.daoId;
    try  {
        const dao = await Dao.findByPk(daoId);
        if (!dao) {
            return res.status(404).send({ message: "Collection Not found." });
        }

        const collection = await dao.getCollection({
            where: {
                daoId: dao.id
            }
        })

        console.log(collection);

        try {
            const collectionUpdated = await compareAndUpdateCollection(collection.helloMoonCollectionId, collection);

            res.status(200).send({collection: collectionUpdated});
        } catch(err) {
            if(err.message) console.log(err.message);
            res.status(400).send({ message : 'A problem occured while updated collection'});
        }


    } catch(err) {
        console.log(err);
        res.status(400).send({ message : 'A problem occured while getting collection'});
    }
}

exports.delete = async (req, res) => {
    const collectionId = req.body.collectionId;
    try {
        const collection = await Collection.findByPk(collectionId);
        collection.update({isDeleted : true});

        res.status(200).send({ message: 'Collection is deleted !'});

    } catch(err) {
        console.log(err);
        res.status(400).send({ message : 'A problem occured while deleting collection'});
    }
}