const axios = require('axios');
const db = require("../models");
const { description } = require('../utils');
const Categorie = db.categories;
const Transaction = db.transaction;

exports.getApiTx = async (req, res) => {

    if (!req.body.address) {
        res.status(400).send({
            message: "No address provided"
        })
    }
    const url = `https://api.helius.xyz/v0/addresses/${req.body.address}/transactions?api-key=${process.env.HELIUS_API_KEY}`;

    try {
        const { data } = await axios.get(url);

        res.status(200).send({
            transactions: data
        })
    } catch (err) {
        res.status(400).send({
            message: err.message || "An error "
        })
    }

    /* TX from Hellomoon are not very sustainable
    const options = {
        method: 'POST',
        url: 'https://rest-api.hellomoon.io/v0/solana/txns-by-user',
        headers: {
            accept: 'application/json',
            'content-type': 'application/json',
            authorization: `Bearer ${process.env.HELLOMOON_API_KEY}`
        },
        data: { userAccount: `${req.body.address}`, limit: 36}
    };

    try {
        
        const { data } = await axios.request(options);
        if (data) {
            const transactions = await Promise.all(data.data.map(async (tx) => {
                const des = await description.createDescription(tx);
                tx.description = des;
                return tx;
            }))

            console.log(transactions);

            res.status(200).send({
                transactions: transactions
            })
        }
    } catch (err) {
        res.status(400).send({
            message: err.message || "An error "
        })
    }*/
}

exports.create = async (req, res) => {
    const transaction = await Transaction.create({
        type: req.body.tx.type,
        source: req.body.tx.source,
        fee: req.body.tx.fee,
        description: req.body.tx.description,
        timestamp: req.body.tx.timestamp,
        events: req.body.tx.events,
        tokenTransfers: req.body.tx.tokenTransfers,
        nativeTransfers: req.body.tx.nativeTransfers,
        accountData: req.body.tx.accountData,
        signature: req.body.tx.signature,
        wallet: req.body.wallet

    });
    const categorie = await Categorie.findByPk(req.body.catId);
    categorie.addTransactions(transaction);

    res.status(200).send({
        "Tx added to catégories ": transaction,
        "categories": categorie
    });
}

exports.getTx = async (req, res) => {
    console.log(req.query.catId);
    const transactions = await Transaction.findAll({
        where: {
            categoryId: req.query.catId
        }
    })
    res.status(200).send({
        "transactions": transactions
    })

}

exports.delete = async (req, res) => {
    try {
        const transaction = await Transaction.findByPk(req.body.txId);
        const categorie = await Categorie.findByPk(req.body.catId);
        const response = await categorie.removeTransactions(transaction);
        if (response) {
            res.status(200).send({
                message: 'Tx have been succesfully delete from categorie'
            })
        }
    } catch (err) {
        res.status(400).send({
            message: err.message
        })
    }

}
